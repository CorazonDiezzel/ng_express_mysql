const express = require('express');
const mysql = require('mysql');
const bodyParser = require('body-parser');
const cors = require('cors');

const db = mysql.createConnection({
  database:'example_db',
  host:'localhost',
  user:'root',
	port:3306,
  password:''
});
db.connect();

const app = express();
app.use(cors());
app.use(bodyParser());

app.get('/',(req, res) => {
  res.send({
    "status":"Working!"
  });
});
app.listen('3000',()=>{
  console.log('Server started');
  ('anjir');
});

//==================Auth
app.post('/auth/login',(req, res) => {
  let sql = "SELECT profileName,email,userId,telp FROM users WHERE username = '"+req.body.username+"' AND password = '"+req.body.password+"'";
    let query = db.query(sql,(err,result) => {
      if(err){
	      console.log('Query Error: ',err);
      }
      const resz = result != null ? result.find(x => x.RowDataPacket !== null):null;
      if(resz){
        if(resz.hasOwnProperty('userId')){
          res.send(result);
        }else{
          res.sendStatus(403);
        }
      }else{
        res.sendStatus(404);
      }
      res.destroy();
    });
});
app.post('/auth/getex',(req, res) => {
  let sql = "SELECT profileName,email,userId,telp,username FROM users WHERE userId = '"+req.body.userId+"'";
    let query = db.query(sql,(err,result) => {
      if(err){
	      console.log('Query Error: ',err);
        throw err;
      }
      const resz = result.find(x => x.RowDataPacket !== null);
      if(resz){
        if(resz.hasOwnProperty('userId')){
          res.send(result);
        }else{
          res.sendStatus(403);
        }
      }else{
        res.sendStatus(404);
      }
      res.destroy();
    });
});
//==================User
function generateuserId(){
  var genId = "";
  var possible = "DEFKLM123450";
  for(var i = 0; i < 14; i++) {
      genId += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return genId;
}
app.post("/reg",(req, res) => {
	let data = {
        userId:generateuserId(),
        username:req.body.userName,
        password:req.body.password,
        email:req.body.email,
        telp:req.body.telp,
        profileName:req.body.profileName
    };
    let sql = "INSERT INTO users SET ?";
      let query = db.query(sql, data,(err, results) => {
      if(err){
		    console.log('data gagal disimpan!',err);
		    throw err;
      }
        res.redirect('/');
      console.log('data berhasil disimpan!');
    });
});
app.post("/update",(req, res) => {
  const dat = req.body;
	let data ={
        userId:dat.userId,
        // username = dat.userName,
        email:dat.email,
        telp:dat.telp,
        profileName:dat.profileName,
        // password:dat.newPassword
      };
    if(dat.userName){data.username=dat.userName;}
    if(dat.newPassword){data.password=dat.newPassword;}
    console.log(data);
    let sql = "UPDATE users SET ? WHERE userId = '"+data.userId+"'";
      let query = db.query(sql, data,(err, results) => {
      if(err){
		    console.log('Perubahan gagal disimpan!',err);
		    throw err;
      }
        res.redirect('/');
      console.log('Perubahan berhasil disimpan!');
    });
    console.log(query.sql);
});
//==================ATK Request
app.post('/get/atkreq',(req, res) => {
  let sql = "SELECT atk_request.id as 'reqId',atk_request.NPP_Pegawai,atk_request.keterangan,bidang.nama_bidang,pegawai.nama_pegawai FROM `atk_request`"
  +" INNER JOIN `bidang` ON atk_request.bidang = bidang.id_bidang INNER JOIN `pegawai` ON atk_request.nama_pegawai = pegawai.id_pegawai"
  +" WHERE atk_request.userId = '"+req.body.userId+"'";
      let query = db.query(sql,(err,results) => {
      if(err){
		  console.log('Query Error: ',err);
		  throw err;
      }
      res.send(results);
    });
});
app.post('/get/atkreqList',(req, res) => {
  let sql = "SELECT atk_list.nama_atk as 'nama',atk_request_list.jumlah as 'jumlah' FROM `atk_request_list` INNER JOIN atk_list ON atk_list.id_atk = atk_request_list.id_atk WHERE atk_request_list.atk_request_id = "+req.body.reqId+"";
      let query = db.query(sql,(err,results) => {
      if(err){
		  console.log('Query Error: ',err);
		  throw err;
      }
      res.send(results);
    });
});
function fetchATKItems(reqId,atkItems){
  let ex = [];
  if(atkItems != null){
     (atkItems).map(x => {
      ex.push([
        null,
        String(reqId),
        String(x.id),
        x.jumlah
      ]
      );
     });
     return ex;
  }else{
    return {};
  }
}
app.post("/save",(req, res) => {
	let data = {
        userId:req.body.userId,
        NPP_pegawai:req.body.npp_pegawai,
        nama_pegawai:req.body.nama_pegawai,
        bidang:req.body.bidang,
        atkLists:req.body.atkLists,
        keterangan:req.body.keterangan
    };
    let atk_r = {
      userId:data.userId,
      NPP_pegawai:data.NPP_pegawai,
      nama_pegawai:data.nama_pegawai,
      bidang:data.bidang,
      keterangan:data.keterangan
    };
    db.beginTransaction(function(err) {
      if (err) { throw err; }
      let a = db.query('INSERT INTO atk_request SET ?',atk_r, function(err, result) {
        if (err) {
          db.rollback(function() {
            console.log(err);
            throw err;
          });
        }
        console.log(result);
        var log = result.insertId;
        let atk_l = fetchATKItems(log,data.atkLists);
        let b = db.query('INSERT INTO atk_request_list VALUES ?',[atk_l], function(err, result) {
          if (err) {
            db.rollback(function() {
              throw err;
            });
          }  
          db.commit(function(err) {
            if (err) { 
              db.rollback(function() {
                throw err;
              });
            }
            res.redirect("/");
          });
        });
      });
    });
});
//==================ATK Entities
app.post('/get/pegawai',(req, res) => {
  let sql = "SELECT * FROM pegawai";
      let query = db.query(sql,(err,results) => {
      if(err){
		  console.log('Query Error: ',err);
		  throw err;
      }
      res.send(results);
    });
});
app.post('/get/bidang',(req, res) => {
  let sql = "SELECT * FROM bidang";
  let query = db.query(sql,(err,results) => {
    if(err){
      console.log('Query Error: ',err);
      throw err;
    }
    res.send(results);  
  });
});
app.post('/get/atkList',(req, res) => {
  const sqlc = req.body.exepts != null ? "WHERE id_atk NOT IN ("+req.body.exepts+")":"";
  let sql = "SELECT * FROM atk_list "+sqlc;
      let query = db.query(sql,(err,results) => {
      if(err){
		  console.log('Query Error: ',err);
		  throw err;
      }
        res.send(results);
    });
});