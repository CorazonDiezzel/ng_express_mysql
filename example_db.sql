-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 22, 2019 at 05:43 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `example_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `atk_list`
--

CREATE TABLE `atk_list` (
  `id_atk` int(11) NOT NULL,
  `nama_atk` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `atk_list`
--

INSERT INTO `atk_list` (`id_atk`, `nama_atk`) VALUES
(1, 'HVS A4 80 gsm (rim)'),
(2, 'HVS A4 70 gsm (rim)');

-- --------------------------------------------------------

--
-- Table structure for table `atk_request`
--

CREATE TABLE `atk_request` (
  `id` int(11) NOT NULL,
  `userId` varchar(15) NOT NULL,
  `NPP_pegawai` varchar(200) NOT NULL,
  `nama_pegawai` int(11) NOT NULL,
  `bidang` int(6) NOT NULL,
  `request_code` int(11) NOT NULL,
  `request_qty` int(12) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `atk_request`
--

INSERT INTO `atk_request` (`id`, `userId`, `NPP_pegawai`, `nama_pegawai`, `bidang`, `request_code`, `request_qty`, `keterangan`) VALUES
(2, '1EMKLE32F355FE', 'wdwd', 2, 4, 2, 4, 'wdwwd'),
(8, '1EMKLE32F355FE', 'wdwd', 2, 6, 1, 7, 'wdwdwdwd');

-- --------------------------------------------------------

--
-- Table structure for table `atk_request_list`
--

CREATE TABLE `atk_request_list` (
  `id` int(11) NOT NULL,
  `atk_request_id` int(11) NOT NULL,
  `id_atk` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `atk_request_list`
--

INSERT INTO `atk_request_list` (`id`, `atk_request_id`, `id_atk`, `jumlah`) VALUES
(1, 2, 1, 4),
(2, 2, 2, 5);

-- --------------------------------------------------------

--
-- Table structure for table `bidang`
--

CREATE TABLE `bidang` (
  `id_bidang` int(6) NOT NULL,
  `nama_bidang` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bidang`
--

INSERT INTO `bidang` (`id_bidang`, `nama_bidang`) VALUES
(1, 'Kepala Cabang'),
(2, 'SDMUKP'),
(3, 'Penagihan dan Keuangan'),
(4, 'Penjamin Manfaat Primer'),
(5, 'Penjamin Manfaat Rujukan'),
(6, 'Perluasan Peserta dan Kepatuhan'),
(7, 'Kepesertaan dan Pelayanan Peserta'),
(8, 'ITHD');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nama_pegawai` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`) VALUES
(1, 'Alexander Ferrick'),
(2, 'Joshua McLaude');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `userId` varchar(15) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telp` varchar(15) NOT NULL,
  `profileName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `userId`, `username`, `password`, `email`, `telp`, `profileName`) VALUES
(5, '1EMKLE32F355FE', 'hahai', 'hwag', 'Haha@haha@gmail.com', '081234567890', 'Hahai');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `atk_list`
--
ALTER TABLE `atk_list`
  ADD PRIMARY KEY (`id_atk`);

--
-- Indexes for table `atk_request`
--
ALTER TABLE `atk_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `atk_request_list`
--
ALTER TABLE `atk_request_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bidang`
--
ALTER TABLE `bidang`
  ADD PRIMARY KEY (`id_bidang`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unik` (`userId`,`username`,`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `atk_list`
--
ALTER TABLE `atk_list`
  MODIFY `id_atk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `atk_request`
--
ALTER TABLE `atk_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `atk_request_list`
--
ALTER TABLE `atk_request_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bidang`
--
ALTER TABLE `bidang`
  MODIFY `id_bidang` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
